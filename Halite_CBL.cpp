#include "hlt/game.hpp"
#include "hlt/constants.hpp"
#include "hlt/log.hpp"
#include <queue>

#include <random>
#include <ctime>
#include <algorithm>
#include <cmath>
#include <deque>
#include <queue>
#include <vector>
#include <set>
#include <unordered_set>
#include <map>

using namespace std;
using namespace hlt;

//MapCell GetRichestCell(){}
enum class Purpose
{
    Stuck = 0,          // must stay put b/c too little halite
    Ram = 1,            // trying to hit an adjacent enemy
    EvadeReturner = 2,  // getting out of the way of someone with the Return purpose
    Flee = 3,           // moving away from an adjacent enemy
    Return = 4,         // bringing halite back to base
    Mine = 5,           // getting halite
    Build = 6,          // Going to build a dropoff
};

struct Plan
{
    shared_ptr<Ship> ship;
    Position tgt;
    Purpose purpose;
};

struct Bot {
    // GLOBAL STATE
    vector<Plan> plans;  // cleared each turn
    set<int> busy_ship_ids;   // ships that shouldn't be assigned new plans. cleared each turn
    unordered_set<int> returning_ship_ids; // maintained across turned
    bool want_dropoff = false;  // recomputed by consider_making_dropoff() each turn
    Position planned_dropoff_pos;
    //map<int, Position> sid_to_impassable;  // recomputed by set_impassable each turn
    map<Position, double> safety_map;
    //Position ram_targets; // squares we are ramming this turn
    bool still_spawning_ships = true;
    bool hasBrutForceReturn = false;

    vector<Command> turn() {

    }
    
};

void AddPlan(vector<Plan>& plans, shared_ptr<Ship> ship, Position tgt, Purpose purpose) {

    //log::log("Ajout d'un plan");
    //log::log("Il y a " + to_string(plans.size()) + " plans");

    for (Plan p : plans) {

        //log::log("p.ship->id" + to_string(p.ship->id));
        //log::log("ship->id" + to_string(ship->id));

        if (p.ship->id == ship->id) {
            //log::log("Duplicats dans les plans");
            return;
        }
    }
    plans.push_back({ ship, tgt, purpose });
    //log::log("Il y a " + to_string(plans.size()) + " plans");
}

Position getBestHaliteMiningPosition(Game& game, std::shared_ptr<Ship> ship) {
    //int radius = 3;
    int BestNumberOfTurnToGetFullAndDrop = 999;
    Position bestHalitePos;

    int HaliteMissing = constants::MAX_HALITE - ship->halite;

    //for (int x = ship->position.x - radius; x <= ship->position.x + radius; x++)
    //{
    //    for (int y = ship->position.y - radius; y <= ship->position.y + radius; y++)
    for (int x = 0; x <= game.game_map->width - 1; x++)
    {
        for (int y = 0; y <= game.game_map->height - 1; y++)
        {
            Position cellPos = { x, y };
            MapCell* cell = game.game_map->at(cellPos);

            //if (ship->reachedDestination() && cell->halite < 3 * game.game_map->at(ship->posTarget)->halite) continue;
            if (cell->halite < game.game_map->at(ship->position)->halite) {
                //log::log("cellule trop pauvre avec : " + to_string(cell->halite) + "tandis que j'ai " + to_string(game.game_map->at(ship->position)->halite));
                continue;
            }

            int HalitePerTurn = cell->halite / constants::EXTRACT_RATIO;
            if (HalitePerTurn < 10) continue;

            int turnToGetFull = (int)(HaliteMissing / HalitePerTurn);
            //int turnToGetFull = 2;

            //for (int i = 1; i < 200; i++) // 200 turn max
            //{
            //    if (HaliteMissing < i * HalitePerTurn) {
            //        turnToGetFull = i;
            //    }
            //}

            //log::log("turnToGetFull " + to_string(turnToGetFull));

            int TurnToGetThere = game.game_map->calculate_distance(cellPos, ship->position);

            int ClosestDistToDropOff = game.game_map->calculate_distance(cellPos, game.me->shipyard->position);

             for (const auto& dropoff_iterator : game.me->dropoffs) {

                 shared_ptr<Dropoff> dropoff = dropoff_iterator.second;

                 int DistToDroppOff = game.game_map->calculate_distance(cellPos, dropoff->position);

                 if (DistToDroppOff <= ClosestDistToDropOff)
                 {
                     ClosestDistToDropOff = DistToDroppOff;
                 }
             }

            int TurnToGetFullAndDeliver = 2 * TurnToGetThere + turnToGetFull + 1 * ClosestDistToDropOff;

            if (TurnToGetFullAndDeliver < BestNumberOfTurnToGetFullAndDrop)
            {
                BestNumberOfTurnToGetFullAndDrop = TurnToGetFullAndDeliver;
                bestHalitePos = cellPos;
                /* log::log("turntogetfullanddeliver " + to_string(TurnToGetFullAndDeliver));
                 log::log("x " + to_string(bestHalitePos.x));
                 log::log("y " + to_string(bestHalitePos.y));
                 log::log("HalitePerTurn " + to_string(HalitePerTurn));*/
            }

            /*   log::log("TurnToGetFullAndDeliver " + to_string(TurnToGetFullAndDeliver));
               log::log("x " + to_string(x));
               log::log("y " + to_string(y));*/

        }
        //log::log("loop " + to_string(x) +"/" + to_string(ship->position.x + radius));

    }
    //log::log("fin ");

    log::log("BestNumberOfTurnToGetFullAndDrop " + to_string(BestNumberOfTurnToGetFullAndDrop));

    return bestHalitePos;
}

Position getRichestCell(Game& game, int radius) {

    //MapCell* richestCell = (MapCell*)malloc(sizeof MapCell);
    Position richestCellPos;

    double maxHalite = 0;

    for (int x = game.me->shipyard->position.x - radius; x <= game.me->shipyard->position.x + radius; x++)
    {
        for (int y = game.me->shipyard->position.y - radius; y <= game.me->shipyard->position.y + radius; y++)
        {
            Position pos = { x, y };
            MapCell* cell = game.game_map->at(pos);
            if (cell->halite > maxHalite)
            {
                maxHalite = cell->halite;
                richestCellPos = cell->position;
            }
        }
    }
    return richestCellPos;
}

bool CanAffordAShip(int halite, bool want_dropoff) {
    int min_halite_to_spawn = constants::SHIP_COST;

    // if we want to build a dropoff but we didn't manage to do it this turn, then
    // only build a ship if we'll have enough halite left after building the ship
    // to also build a dropoff
    if (want_dropoff) min_halite_to_spawn += constants::DROPOFF_COST;
    return halite >= min_halite_to_spawn;
}

bool WantToBuildAShip(int turn_number) {
    return (turn_number < 3 * constants::MAX_TURNS / 4);
}

bool WantToBuildDropOff(int turn_number, int dropoffCount, int shipCount) {


    //TODO
    // 
    return false;
    //log::log("turn number : " + to_string(turn_number));
    bool want_dropoff = false;


    //log::log("turn_number > 3 * constants::MAX_TURNS / 4 : " + to_string(turn_number > 3 * constants::MAX_TURNS / 4));

    if (turn_number > 3 * constants::MAX_TURNS / 4) {
        return false;
    };

    int DesiredNumberOfDropOff = (shipCount - 12) / 4;
    log::log("DesiredNumberOfDropOff : " + to_string(DesiredNumberOfDropOff));

    return (dropoffCount < DesiredNumberOfDropOff);
}

int TileAverageHalite(Game& game, Position tilePosition, int radius)
{
    int halite = 0;

    for (int x = tilePosition.x - radius; x < tilePosition.x + radius; x++)
    {
        for (int y = tilePosition.y - radius; y < tilePosition.y + radius; y++)
        {
            Position cellPos = { x, y };
            MapCell* cell = game.game_map->at(cellPos);
            halite += cell->halite;
        }
    }
    return halite;
}

Position bestDropOffPosition(Game& game, Position shipYardPos, int radius) {
    int bestHalite = 0;
    Position bestHaliteTile;

    for (int x = shipYardPos.x - radius; x <= shipYardPos.x + radius; x++)
    {
        for (int y = shipYardPos.y - radius; y <= shipYardPos.y + radius; y++)
        {
            Position cellPos = { x, y };
            //MapCell* cell = game.game_map->at(cellPos);

            if (TileAverageHalite(game, cellPos, 4) > bestHalite)
            {
                bestHaliteTile = cellPos;
            };
        }
    }
    return bestHaliteTile;
}

shared_ptr<Ship> closestShipToPosition(Game& game, Position pos, std::unordered_map<EntityId, std::shared_ptr<Ship>> Ships) {
    int closestDistance = 999;

    shared_ptr<Ship> ClosestShip;

    for (const auto& ship_iterator : Ships) {
        shared_ptr<Ship> ship = ship_iterator.second;
        if (game.game_map->calculate_distance(pos, ship->position) < closestDistance)
        {
            ClosestShip = ship;
        };
    }
    return ClosestShip;
}

//struct PositionSnapshot
//{
//    int ship_id;
//    Position position;
//    Position targetPos;
//};


int main(int argc, char* argv[]) {
    unsigned int rng_seed;
    if (argc > 1) {
        rng_seed = static_cast<unsigned int>(stoul(argv[1]));
    } else {
        rng_seed = static_cast<unsigned int>(time(nullptr));
    }
    mt19937 rng(rng_seed);

    Game game;
    // At this point "game" variable is populated with initial map data.
    // This is a good place to do computationally expensive start-up pre-processing.
    // As soon as you call "ready" function below, the 2 second per turn timer will start.
    game.ready("MyCppBot");

    log::log("Successfully created bot! My Player ID is " + to_string(game.my_id) + ". Bot rng seed is " + to_string(rng_seed) + ".");
    log::log("constants::EXTRACT_RATIO " + to_string(constants::EXTRACT_RATIO));

    shared_ptr<Player> me = game.me;
    unique_ptr<GameMap>& game_map = game.game_map;


    vector<Plan> plans;
    vector<Plan> newPlans;

    //vector<PositionSnapshot> ShipSnapShot;


    int builder_ship_id;

    for (;;) {
        game.update_frame();

        // On nettois les plans
        newPlans.clear();

        vector<Command> command_queue;

        // On regarde si les vaisseaux ont r�ussi � se d�placer
        // Si ce n'est pas le cas, on les mets dans l'array
        vector<shared_ptr<Ship>> StuckShips;

        // Pour chaque plans, 
        for (Plan p : plans) {
            for (const auto& ship_iterator : me->ships) {
                shared_ptr<Ship> ship = ship_iterator.second;

                // On trouve le navire
                if (ship->id == p.ship->id)
                {
                    // Si le vaisseau ne doit pas rentrer 
                    if (p.purpose == Purpose::Return) continue;

                    // Si le vaisseau n'est pas � son endroit cibl�
                    if (ship->position.equals(p.tgt)) continue;
                    
                    // Si le vaisseau n'as pas boug�
                    if (ship->position.equals(p.ship->position)) {
                        // Choix d'une case adjacente random
                        AddPlan(newPlans, ship, ship->position.get_surrounding_cardinals()[rng() % 4], Purpose::EvadeReturner);
                    }
                }
            }
        }

        /*for (const auto& ship_iterator : me->ships) {
            shared_ptr<Ship> ship = ship_iterator.second;
            if (game_map->at(ship)->halite < constants::MAX_HALITE / 1 || ship->is_full()) {
                Direction random_direction = ALL_CARDINALS[rng() % 4];
                command_queue.push_back(ship->move(random_direction));
            } else {
                command_queue.push_back(ship->stay_still());
            }
        }*/

        Position bestDroppoffPos = bestDropOffPosition(game, me->shipyard->position, game.game_map->width/10);
        bool wantDropoff = WantToBuildDropOff(game.turn_number, me->dropoffs.size(), me->ships.size());
        
        //Redefinir le but des vaisseaux

        //Premi�rement, on ajoute les retour en cours, ceux-ci garderont l'ordre de retourner

        for (Plan p : plans) {
            if (p.purpose == Purpose::Return) 
            {
                for (const auto& ship_iterator : me->ships) {
                    shared_ptr<Ship> ship = ship_iterator.second;

                    //Mise a jour de la valeur d'Halite du navire
                    if (ship->id == p.ship->id)
                    {
                        // Si il as �t� trouv� et est toujours plein, on maintient l'ordre de d�pot
                        if (ship->halite > constants::MAX_HALITE / 2)
                        {
                            //log::log("Ajout d'un retour avant toute chose (news plans n'est plus vide)");
                            newPlans.push_back({ ship, p.tgt, p.purpose });
                        }
                    }
                }
            }
        }

        //Ensuite, ordre de d�placement random Des vaisseaux coinc�s
        for (const auto& ship_iterator : me->ships) {
            shared_ptr<Ship> ship = ship_iterator.second;

            //for (PositionSnapshot snapShot : ShipSnapShot)
            //{
            //    if (ship->id == snapShot.ship_id)
            //    {
            //        if (ship->position == snapShot.position)
            //        {


            //            // Le vaisseau n'a pas boug�
            //            Position newTarget = ship->position.get_surrounding_cardinals()[rng() % 4];
            //            
            //            newPlans.push_back({ ship, {}, Purpose::EvadeReturner });
            //        }
            //    }
            //}
        }
        // 
        // 
        //log::log("Il y a " + to_string(newPlans.size()) + " plans de retour");

        // Chaque vaisseau gagne un nouvel ordre
        for (const auto& ship_iterator : me->ships) {
            shared_ptr<Ship> ship = ship_iterator.second;

            Purpose newPurpose = Purpose::Mine;

            if (wantDropoff && closestShipToPosition(game, bestDroppoffPos, me->ships)->position.equals(ship->position)) {
                //log::log(ship->to_string() + "Je suis le plus proche");
                newPurpose = Purpose::Build;
            }
            else if (ship->is_full()) {
                //log::log("Je dois rentrer avec " + to_string(ship->halite));
                newPurpose = Purpose::Return;
            }
            else if (ship->halite <= constants::MAX_HALITE / 100)
            {
                //log::log("ID : "+ to_string(ship->id) + " - Je dois aller miner ");
                newPurpose = Purpose::Mine;
            }

            // Plannifier la destination en fonction du but

            Position targetPos;

            switch (newPurpose)
            {
                case Purpose::Build:
                    targetPos = bestDroppoffPos;
                    //log::log("id : " + to_string(ship->id) + " going to build on : " + bestDroppoffPos.to_string());
                    if (bestDroppoffPos.equals(ship->position))
                    {
                        if (me->halite >= constants::DROPOFF_COST)
                        {
                            command_queue.push_back(ship->make_dropoff());
                        }
                        continue;
                    }
                    break;

                case Purpose::Return:
                    targetPos = me->shipyard->position;
                    //log::log(ship->to_string() + " - Je dois rentrer : " + targetPos.to_string());
                    break;

                case Purpose::Mine:
                    targetPos = getBestHaliteMiningPosition(game, ship);
                    //log::log("ID : " + to_string(ship->id) + " - Je dois aller miner ici : " + targetPos.to_string());
                    //log::log(ship->to_string() + " - Je dois aller miner ici : " + targetPos.to_string());
                    break;
                //default:
                //    break;
            }

            // Ajouter la plannification du vaisseau, verification qu'un vaisseau ne peut pas avoir 2 ordres (cela est utile car une fois assign� � un retour, il y reste)
            AddPlan(newPlans, ship, targetPos, newPurpose);
        }


         //D�finir le nouveau tour avec les plans �tablis
        for (Plan p : newPlans) {
            //log::log("ID : " + to_string(p.ship->id) + " - Dest : " + p.tgt.to_string() + " - Purpose " + to_string((int)p.purpose));
            log::log(p.ship->to_string()+ " - Purpose " + to_string((int)p.purpose) + " Going to : " + p.tgt.to_string());

            // Qu'importe l'ordre, si le vaisseaux est d�ja sur sa position, il attend
            if (p.tgt.equals(p.ship->position))
            {
                continue;
            }

            // Risky move ne marque pas la case du vaisseau comme occup� lors d'un retour
            command_queue.push_back(p.ship->move(game_map->naive_navigate(p.ship, p.tgt)));
        }

        // assignation des nouveaux plans au vecteur des anciens et nettoyage des anciens pour la suite
        plans.clear();
        for (Plan p : newPlans) {
            AddPlan(plans, p.ship, p.tgt, p.purpose);
        }
        newPlans.clear();

        //ship->lastPos = ship->position;

        if (
            game.turn_number <= 3 * constants::MAX_TURNS / 4 &&
            me->halite >= constants::SHIP_COST &&
            !game_map->at(me->shipyard)->is_occupied())
        {
            if (!wantDropoff)
            {
                if (WantToBuildAShip)
                {
                    //if (me->ships.size() <3)
                    //{
                        command_queue.push_back(me->shipyard->spawn());
                    //}
                }
            }
        }

        if (!game.end_turn(command_queue)) {
            break;
        }
    }

    return 0;
}
